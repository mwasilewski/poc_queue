# import sys
import eventlet
from eventlet.green import zmq
from eventlet import sleep

# Config
# db
MYSQL_HOST = '172.17.0.2'
MYSQL_USER = 'root'
MYSQL_PASSWORD = 'mypass'
MYSQL_DB = 'poc'
# hosts
API_HOST = 'api'
API_PORT = 6000
QUEUE_HOST = 'queue'
QUEUE_PORT = 7000
# worker
RESPONSE_PORTS = [8001, 8002]
JOB_TIMEOUT = 30  # sec


class PortScheduler(object):

    def __init__(self):
        self.q = eventlet.Queue()
        for p in RESPONSE_PORTS:
            self.q.put(p)


def db_sync():
    """
    Connects to db and searches for all entry
    marked as unprocessed.
    TODO: when to do it? only on start?
    """
    pass


def listen_api(context, job_queue):
    consumer_receiver = context.socket(zmq.PULL)
    consumer_receiver.connect("tcp://{}:{}".format(API_HOST, API_PORT))
    while True:
        print "Waiting for API job...{}".format(type(job_queue))
        msg = consumer_receiver.recv_json()
        job_queue.put(msg)


def listen_worker(context, job_queue, port_queue, job):
    port = job['port']
    worker_receiver = context.socket(zmq.PULL)
    worker_receiver.bind("tcp://{}:{}".format(QUEUE_HOST, port))
    poller = zmq.Poller()
    poller.register(worker_receiver, zmq.POLLIN)
    cnt = JOB_TIMEOUT
    while True:
        if poller.poll(1):
            # poll is blocking call (1sec), will be not enough in the high load
            response = worker_receiver.recv_json()
            print("Received response from worker: {}".format(response))
            if response.get('status') == 'OK':
                worker_receiver.close()
                port_queue.put(port)
            else:
                worker_receiver.close()
                port_queue.put(port)
                job_queue.put(job)
            break
        sleep(1)
        cnt -= 1
        if cnt <= 0:
            worker_receiver.close()
            port_queue.put(port)
            job_queue.put(job)
            break


def queue():
    """
    Rough design rules:
    - thread for listening and putting on a job queue
    - thread for sending and managing job statuses
    - syncing part taking entries from DB which
      does not have result column filled in.
    """
    print("Starting queue on: {}:{}. Listening from API: {}:{}".format(
        QUEUE_HOST, QUEUE_PORT, API_HOST, API_PORT))
    job_queue = eventlet.Queue()
    pool = eventlet.GreenPool()
    context = zmq.Context()
    pool.spawn_n(listen_api, context, job_queue)

    port_sched = PortScheduler()
    consumer_sender = context.socket(zmq.PUSH)
    consumer_sender.bind("tcp://{}:{}".format(QUEUE_HOST, QUEUE_PORT))
    while True:
        print "Waiting for msg..."
        msg = job_queue.get()
        print "Waiting for free worker...: {}".format(msg)
        port = port_sched.q.get()
        msg['port'] = port
        consumer_sender.send_json(msg)
        pool.spawn_n(listen_worker, context, job_queue, port_sched.q, msg)


if __name__ == "__main__":
    queue()
