FROM python:3
RUN pip install zmq
ADD queue.py /
CMD [ "python", "./queue.py" ]
